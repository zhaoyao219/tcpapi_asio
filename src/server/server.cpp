#include <iostream>
#include <string>
#include <thread>
#include <functional>
#include "tcp_api.h"

int64_t g_cout = 0;

class CServerSpi : public CTcpSpi
{
public:
	CServerSpi(CTcpApi* api) :m_api(api) 
	{
		//m_api->AddTimer(1, 1000, std::bind(&CServerSpi::OnTimer,this));
	}
	virtual void OnConnected(Session_t session)
	{
		printf("OnConnected:session_id=%d", session.nSessionID);
	}
	virtual void OnDisConnect(Session_t session)
	{
		printf("OnDisConnect:session_id=%d\n", session.nSessionID);
	}
	virtual void OnMessage(Session_t session, const std::string msg)
	{
		g_cout++;
		printf("OnMessage:recv data:[%d-%s]\n", session.nSessionID,msg.c_str());
		m_api->Write(session, msg.data(), msg.size());
	}
	void OnTimer()
	{
		printf("call ontime test msg\n");
	}
private:
	CTcpApi* m_api;
};

//服务端测试
int main(int argc, char* argv[])
{
	auto api = new CTcpApi();
	auto spi = new CServerSpi(api);
	api->RegisterSpi(spi, nullptr);
	api->StartServer(8089);
	std::this_thread::sleep_for(std::chrono::seconds(3));
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	api->StopWork();
	if (spi)
	{
		delete spi;
	}
	if (api)
	{
		delete api;
	}
	return 0;
}