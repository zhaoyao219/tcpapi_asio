#include <iostream>
#include <string>
#include <thread>
#include "tcp_api.h"

int64_t g_cout = 0;

class CClientSpi : public CTcpSpi
{
public:
	CClientSpi(CTcpApi* api):m_api(api){}
	virtual void OnConnected(Session_t session)
	{
		printf("OnConnected:session_id=%d\n", session.nSessionID);
		std::string msg1 = "I am client,send times:" + std::to_string(g_cout);
		m_api->Write(session, msg1.data(), msg1.size());
	}
	virtual void OnDisConnect(Session_t session)
	{
		printf("OnDisConnect:session_id=%d\n", session.nSessionID);
	}
	virtual void OnMessage(Session_t session, const std::string msg)
	{
		std::this_thread::sleep_for(std::chrono::seconds(2));
		g_cout++;
		printf("client_id=[%d]send msg\n", session.nSessionID);
		std::string msg1 = "send times:" + std::to_string(g_cout);
		m_api->Write(session, msg1.data(), msg1.size());
	}
private:
	CTcpApi* m_api;
};

//客户端测试
int main(int argc, char* argv[])
{
	auto api = new CTcpApi();
	auto spi = new CClientSpi(api);
	api->RegisterSpi(nullptr,spi);
	api->StartConnect("127.0.0.1", 8089, true);
	std::this_thread::sleep_for(std::chrono::seconds(3));
	while (true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1));
	}
	api->StopWork();
	if (spi)
	{
		delete spi;
	}
	if (api)
	{
		delete api;
	}
	return 0;
}