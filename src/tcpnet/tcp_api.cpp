#include "tcp_api.h"
#include "channel_manager.h"
#include "log.h"

logger_t* g_log = nullptr;

CTcpApi::CTcpApi(int nLogLevel, int nLogMode)
{
	m_ChannelManager = std::make_shared<ChannelManager>();
	g_log = default_logger();
	log_set_level(nLogLevel);
	log_set_mode(nLogMode);
	log_every_flush();//每次打印都flush，测试情况下方便实时查看日志
}
CTcpApi::~CTcpApi()
{
	logger_destroy(g_log);
}

void CTcpApi::RegisterSpi(CTcpSpi* pSvrSpi, CTcpSpi* pCliSpi)
{
	m_pSvrSpi = pSvrSpi;
	m_pCliSpi = pCliSpi;
}

void CTcpApi::StartServer(int32_t port)
{
	log_set_file("server.log");
	LOGI("Tcp client start work!!!");
	if (!m_pSvrSpi)
	{
		LOGW("server Spi is null");
	}
	if (!m_ChannelManager)
	{
		LOGE("channelMgr is null");
		return;
	}
	m_ChannelManager->StartListen(port, m_pSvrSpi);
}

int32_t CTcpApi::StartConnect(const char *szHost, int32_t sPort, bool bAutoReLink)
{
	log_set_file("client.log");
	LOGI("Tcp client start work!!!");
	if (!m_pCliSpi)
	{
		LOGW("client Spi is null\n");
	}
	if (!m_ChannelManager)
	{
		LOGE("channelMgr is null");
		return -1;
	}
	if (!m_ChannelManager->Connect(szHost, sPort, m_pCliSpi, bAutoReLink))
	{
		return -1;
	}
	
	return 0;
}

void CTcpApi::StopWork()
{
	if (!m_ChannelManager)
	{
		LOGE("channelMgr is null");
		return;
	}
	m_ChannelManager->Stop();
}

int32_t CTcpApi::Write(Session_t session, const char* pData, int64_t nDataLen)
{
	m_ChannelManager->SendByChannelID(session.nSessionID,pData,nDataLen);
	return 0;
}

void CTcpApi::AddTimer(int id, int ms, mfvoid cb)
{
	m_ChannelManager->AddTimer(id, ms, cb);
}

void CTcpApi::RemoveTimer(int id)
{
	m_ChannelManager->RemoveTimer(id);
}

//void CTcpApi::RegisterLog(int level, CallBackLog fn)
// {
//	logger_set_handler(g_log,fn);
//}