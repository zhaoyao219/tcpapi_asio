﻿@echo off
::递归删除
@for /r . %%a in (.) do @if exist "%%a\.vs" rd /s /q "%%a\.vs"
@for /r . %%a in (.) do @if exist "%%a\x64" rd /s /q "%%a\x64"
@for /r . %%a in (.) do @if exist "%%a\Debug" rd /s /q "%%a\Debug"

::单独删除
del /F /S /Q *.sdf *.suo *.pdb *.ilk *.iobj *.ipdb *.log *.exp 
rd /q /s "bin"

::start  "D:\Program Files\Git\git-bash.exe"