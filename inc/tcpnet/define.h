#pragma once

#include <stdint.h>

#ifdef _WIN32
#define DIR_SEPARATOR       '\\'
#define DIR_SEPARATOR_STR   "\\"
#else
#define DIR_SEPARATOR       '/'
#define DIR_SEPARATOR_STR   "/"
#endif

#ifndef __FILENAME__
#define __FILENAME__  (strrchr(DIR_SEPARATOR_STR __FILE__, DIR_SEPARATOR) + 1)
#endif

#pragma pack(push,1)

struct NetHead 
{
	int nMagic;
	int nMsgType;
	int64_t nHeadSize;
	int64_t nBodySize;
	int64_t nSerialNo;
};

//当前连接的上下文信息
struct Session_t 
{
	int32_t nSessionID;//channel 在map中的key
	char chRemoteIP[64];
	int32_t nRemotePort;
};

enum MsgType
{
	ShakeHand = 1,
	MsgHeartBeat, // 心跳消息
	MsgContext = 101 // 正常通信了
};

// 通道类型
enum ChannalType
{
	TCPSERVER,//主动
	TCPCLIENT //被动
};

enum EncryptMethod
{
	Encrypt_None,
	Encrypt_3des,
};

//握手请求包体
struct ShakeHandReq
{
	int nServiceID; // 废弃
};

//握手应答包体
struct ShakeHandRsp
{
	int32_t nClientID;
	int32_t nServerID; // 回应方的ServiceID
	int nResult;
	char chInfo[1024];
};

// 心跳消息
struct HeartBeatReq
{
	int nChannelID; //当前连接的sessionID
	char data[32]; 
};

#pragma pack(pop)