#pragma once
#include "define.h"
#include <stdarg.h>
#include <functional>
#include <memory>
#include <string>
#include <stdint.h>

//生成动态库
#ifdef TCP_API_EXPORTS
#define TCP_API_LIB_API __declspec(dllexport)
#else
#ifdef WIN_DLL
#define TCP_API_LIB_API __declspec(dllimport)
#else
#define TCP_API_LIB_API 
#endif
#endif

using mfvoid = std::function<void()>;

class CTcpSpi
{
public:
	virtual void OnConnected(Session_t session) = 0;
	virtual void OnDisConnect(Session_t session) = 0;
	virtual void OnMessage(Session_t session, const std::string msg) = 0;
};

class ChannelManager;
class TCP_API_LIB_API CTcpApi 
{
public:
	//************************************
	// Method:    CTcpApi
	// Author:    zhaoyao
	// DATE:      2021/07/26 13:29:39
	// Brief:     loglevel=[0:TRACE][1:DEBUG][2:INFO][3:WARN][4:ERROR][5:FATAL][6:NONE]
	// Brief:     nLogMode=[0:日志输出屏幕及文件][1:日志只输出文件][2:日志只输出屏幕]
	//************************************
	CTcpApi(int nLogLevel = 0,int nLogMode = 0);
	~CTcpApi();
	//************************************
	// 函数名: RegisterSpi
	// 作  者: 赵耀
	// 时  间: 2021/07/24 21:52:58
	// 描  述: 注册回调，接收应答及事件，参数一作为服务端时的回调指针，参数二作为客户端时的回调指针
	//************************************
	void RegisterSpi(CTcpSpi* pSvrSpi = nullptr, CTcpSpi* pCliSpi = nullptr);

	//************************************
	// 函数名: StartServer
	// 作  者: 赵耀
	// 时  间: 2021/07/24 21:23:25
	// 描  述: 作为服务端监听端口
	//************************************
	void StartServer(int32_t port);

	//************************************
	// 函数名: StartConnect
	// 作  者: 赵耀
	// 时  间: 2021/07/24 21:24:45
	// 描  述: 作为客户端主动向服务端发起连接请求,可自定义是否断线自动重连
	//************************************
	int32_t StartConnect(const char* host, int32_t port, bool bAutoReLink = false);
	//************************************
	// 函数名: StopWork
	// 作  者: 赵耀
	// 时  间: 2021/07/24 21:27:55
	// 描  述: 主动停止API工作引擎，恢复工作需要重新执行StartServer或StartConnect
	//************************************
	void StopWork();
	//************************************
	// 函数名: Write
	// 作  者: 赵耀
	// 时  间: 2021/07/25 0:48:38
	// 描  述: 根据sessionID向对端发送数据
	//************************************
	int32_t Write(Session_t session, const char* pData, int64_t nDataLen);
	//************************************
	// 函数名: AddTimer
	// 作  者: 赵耀
	// 时  间: 2021/07/25 2:10:59
	// 描  述: 添加定时器任务，id是定时器的唯一标识，ms定时器时间间隔毫秒值，cb定时器定时回调的函数
	//************************************
	void AddTimer(int id, int ms, mfvoid cb);
	//************************************
	// 函数名: RemoveTimer
	// 作  者: 赵耀
	// 时  间: 2021/07/25 22:51:46
	// 描  述: 移除指定的定时器
	//************************************
	void RemoveTimer(int id);
private:
	std::shared_ptr<ChannelManager> m_ChannelManager;//通道管理器
	CTcpSpi* m_pSvrSpi;//作为服务端时，事件和消息回调接口
	CTcpSpi* m_pCliSpi;//作为客户端时，事件和消息回调接口
};
